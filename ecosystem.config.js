module.exports = {
  apps: [{
    name: 'Condor Web',
    version: '2.0.0',
    script: './dist/condor-web/server/main.js',
    instances: 1,
    autorestart: true,
    watch: true,
    max_memory_restart: '300M',
    env: {
      NODE_ENV: 'development',
    },
    env_production: {
      NODE_ENV: 'production',
    },
  }, ],
};
