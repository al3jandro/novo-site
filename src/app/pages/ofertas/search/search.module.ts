import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { Routes, RouterModule } from '@angular/router';
import { PipesModule } from '../../../shared/pipes/pipes.module';
import { HelperModule } from 'src/app/shared/helper/helper.module';
import { OfertasModule } from 'src/app/shared/components/ofertas/ofertas.module';


const app: Routes = [
  { path: ':slug', component: SearchComponent }
];


@NgModule({
  declarations: [SearchComponent],
  imports: [
    PipesModule,
    HelperModule,
    CommonModule,
    MDBBootstrapModule,
    OfertasModule,
    RouterModule.forChild(app)
  ]
})
export class SearchModule { }




