import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/shared/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { UtilService } from '../../../shared/services/util.service';
import { map, tap } from 'rxjs/operators';
import { SeoService } from '../../../shared/services/seo.service';
import { Api2Service } from 'src/app/shared/services/api2.service';

@Component({
  selector: 'app-departamento',
  templateUrl: './departamento.component.html',
  styleUrls: ['./departamento.component.scss']
})
export class DepartamentoComponent implements OnInit, OnDestroy {

  subscription: Subscription;

  p: number = 1;
  search: string = '';

  viewDepart: boolean = true;
  viewSector: boolean = true;
  init: boolean = false;

  loja: any = [];
  menu: any = [];
  items: any = [];
  sector: any = [];
  departamento: any = [];

  slug: Observable<string>;

  constructor(
    private act: ActivatedRoute,
    private util: UtilService,
    private api: ApiService,
    private v2: Api2Service,
    private router: Router,
    private seo: SeoService
  ) { }

  ngOnInit(): void {
    this.loja = this.util.StorageParse('Loja');
    this.slug = this.act.paramMap.pipe(map(paramsMap => paramsMap.get('slug')));
    this.slug.subscribe(data => {
      const url = this.router.url.split('/');
      const value = url[url.length - 1];
      if (value.split('?')[0] === 'init') { this.init = true; }
      this.Departamento(`${data}`, this.loja.loja);
    });
  }


  ngOnDestroy(): void {
    if (this.subscription) { this.subscription.unsubscribe(); }
  }

  Departamento(slug: string, loja: number) {
    this.departamento = this.v2.getCollection(`/Produtos/menuDepartamentoSlug?slug=${slug}`)
    .pipe(map((data) => data[0]), tap(res => {
      this.MenuSector(loja, res.codigo);
      this.Seo(res.nome);
      this.items = this.v2.getCollection(`/Ofertas/LojaProdutosDepartamento?loja=${loja}&departamento=${res.codigo}`);
    }))
    this.MenuDepartamento(loja);
  }

  MenuSector(loja: number, departamento: number) {
    const query = `/Menus/MenuSectorOfertasLojaDepartamento?loja=${loja}&departamento=${departamento}`;
    this.sector = this.v2.getCollection(query);
  }

  MenuDepartamento(loja: number) {
    const query = `/Menus/MenuDepartamentoOfertasLojaDepartamento?loja=${loja}`;
    this.menu = this.v2.getCollection(query);
  }

  onSearch(e: any) {
    console.log(e.target.value);
  }
  toogleSector() { this.viewSector = !this.viewSector; }
  toogleDepart() { this.viewDepart = !this.viewDepart; }

  private Seo(departamento: string) {
    this.seo.setTitle(`${departamento} | Rede Condor`);
    this.seo.updateTagName(`${departamento} | Rede Condor`);
    this.seo.addCanonical();
    this.seo.dataLayerProduct({
      event: 'page',
      pageType: 'Ofertas',
      productCategory: departamento,
      pageTitle: departamento
    });
  }
}
