import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilService } from 'src/app/shared/services/util.service';
import { ApiService } from 'src/app/shared/services/api.service';
import { Subscription, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SeoService } from '../../../shared/services/seo.service';


@Component({
  selector: 'app-sector',
  templateUrl: './sector.component.html',
  styleUrls: ['./sector.component.scss']
})
export class SectorComponent implements OnInit, OnDestroy {

  subscription: Subscription;

  p: number = 1;
  search: string = '';

  viewDepart: boolean = true;
  viewSector: boolean = true;
  init: boolean = false;

  loja: any = [];
  menu: any = [];
  items: any = [];
  sector: any = [];
  path: any = [];
  url: any = [];

  slug: Observable<string>;

  constructor(
    private act: ActivatedRoute,
    private util: UtilService,
    private api: ApiService,
    private router: Router,
    private seo: SeoService
  ) { }

  ngOnInit(): void {
    this.loja = this.util.StorageParse('Loja');
    this.slug = this.act.paramMap.pipe(map(paramsMap => paramsMap.get('slug')));
    this.slug.subscribe(data => {
      const url = this.router.url.split('/');
      const value = url[url.length - 1];
      if (value.split('?')[0] === 'init') { this.init = true; }
      this.Sector(`${data}`);
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) { this.subscription.unsubscribe(); }
  }

  Sector(slug: string) {
    const arr = [];
    this.url = {
      nome: this.router.url.split('/')[2],
      link: this.util.toSlug(this.router.url.split('/')[2])
    };
    this.subscription = this.api.getMenuOfertas(`menuSetorSlug?slug=${slug}`).subscribe(
      menu => {
        this.path = menu[0];
        this.MenuDepartamneto(this.loja.loja);
        this.MenuSector(this.loja.loja, this.path.dep_id);
        this.api.OfertasLojaDepartamentoSetor(this.loja.loja, this.path.dep_id, menu[0].codigo)
        .subscribe(
          (res) => {
            res.forEach((element) => {
              if (element.produtos) {
                arr.push(element);
              }
            });
          },
          (err) => console.log(err)
        );
        this.items = arr;
        this.Seo(menu[0].nome);
      },
      err => console.log(err)
    );
  }

  MenuSector(loja: number, departamento: number) {
    const query = `/Menus/MenuSectorOfertasLojaDepartamento?loja=${loja}&departamento=${departamento}`;
    this.api.getCollection(query).subscribe(
      res => this.sector = res,
      err => console.log(err)
    );
  }

  MenuDepartamneto(loja: number) {
    const query = `/Menus/MenuDepartamentoOfertasLojaDepartamento?loja=${loja}`;
    this.api.getCollection(query).subscribe(
      res => this.menu = res,
      err => console.log(err)
    );
  }

  toogle() { this.viewSector = !this.viewSector; }
  busca(e: any) { this.search = e.target.value; }

  private Seo(departamento: string) {
    this.seo.setTitle(`${departamento} | Rede Condor`);
    this.seo.updateTagName(`${departamento} | Rede Condor`);
    this.seo.addCanonical();
    this.seo.dataLayerProduct({
      event: 'page',
      pageType: 'Ofertas',
      productCategory: 'Setor',
      pageTitle: departamento
    });
  }
}
