import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NewsService } from '../../../shared/services/news.service';
import { UtilService } from 'src/app/shared/services/util.service';
import { ApiService } from 'src/app/shared/services/api.service';
import { Subscription, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { SeoService } from '../../../shared/services/seo.service';
import { Api2Service } from 'src/app/shared/services/api2.service';


@Component({
  selector: 'app-campanha',
  templateUrl: './campanha.component.html',
  styleUrls: ['./campanha.component.scss']
})
export class CampanhaComponent implements OnInit, OnDestroy {

  subscription: Subscription;

  p: number = 1;
  search: string = '';
  str: number;

  view: boolean = true;
  init: boolean = false;

  loja: any = [];
  menu: any = [];
  items: any = [];
  campanha: Observable<any>;
  departamento: any = [];

  slug: Observable<string>;

  constructor(
    private act: ActivatedRoute,
    private news: NewsService,
    private util: UtilService,
    private api: ApiService,
    private v2: Api2Service,
    private seo: SeoService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loja = this.util.StorageParse('Loja');
    this.slug = this.act.paramMap.pipe(map(paramsMap => paramsMap.get('slug')));
    this.slug.subscribe(data => {
      const url = this.router.url.split('/');
      const value = url[url.length - 1];
      if (value.split('?')[0] === 'init') { this.init = true; }
      this.Campanha(`${data}`, this.loja.loja);
    });
  }


  ngOnDestroy(): void {
    if (this.subscription) { this.subscription.unsubscribe(); }
  }

  Campanha(slug: string, loja: number) {
    console.log(slug);
    this.campanha = this.news.Campanha(slug).pipe(tap(res => {
      this.Seo(res.title);
      if (res.code) {
        this.MenuDepartamentoCampanha(this.loja.loja, res.code);
        this.items = this.v2.getCollection(`/Ofertas/LojaProdutosCampanha?loja=${loja}&campanha=${res.slugCampanha}`);
        this.api.OfertasLojaCampanha(this.loja.loja, res.code, 60).subscribe(
          dat => this.items = dat,
          err => console.log(err)
        );
      } else {
        this.MenuDepartamentoSlug(this.loja.loja, res.slugCampanha);
        this.items = this.v2.getCollection(`/Ofertas/LojaProdutosSlug?loja=${loja}&slug=${res.slugCampanha}`);
      }
    }))
  }

  MenuDepartamentoSlug(loja: number, slug: string) {
    const query = `/Menus/MenuDepartamentoOfertasLojaSlug?loja=${loja}&slug=${slug}`;
    this.departamento = this.v2.getCollection(query);
  }

  MenuDepartamentoCampanha(loja: number, campanha: number) {
    const query = `/Menus/MenuDepartamentoOfertasLojaCampanha?loja=${loja}&campanha=${campanha}`;
    this.departamento = this.v2.getCollection(query);
  }

  toogle(str: number) {
    this.str = str;
    this.view = !this.view;
  }

  private Seo(departamento: string) {
    this.seo.setTitle(departamento);
    this.seo.addTagName(departamento);
    this.seo.addCanonical();
    this.seo.dataLayerProduct({
      event: 'page',
      pageType: 'Ofertas',
      productCategory: 'Campanha',
      pageTitle: departamento
    });
  }
}
