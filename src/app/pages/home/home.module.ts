
import { PostModule } from './../../shared/components/post/post.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HOME_ROUTE } from './home.routes';
import { ResponsiveModule } from 'ngx-responsive';
import { HelperModule } from 'src/app/shared/helper/helper.module';
import { CarouselModule } from './../../shared/components/carousel/carousel.module';
import { OfertasModule } from './../../shared/components/ofertas/ofertas.module';
import { BannersModule } from './../../shared/components/banners/banners.module';
import { InstitucionalModule } from './../../shared/components/institucional/institucional.module';

const config = {
  breakPoints: {
      xs: {max: 600},
      sm: {min: 601, max: 959},
      md: {min: 960, max: 1279},
      lg: {min: 1280, max: 1919},
      xl: {min: 1920}
  },
  debounceTime: 100
};

@NgModule({
  declarations: [HomeComponent],
  imports: [
    HelperModule,
    CommonModule,
    CarouselModule,
    OfertasModule,
    PostModule,
    InstitucionalModule,
    BannersModule,
    HOME_ROUTE,
    ResponsiveModule.forRoot(config)
  ]
})
export class HomeModule { }
