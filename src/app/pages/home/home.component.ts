import { Component, OnInit, OnDestroy } from '@angular/core';
import { UtilService } from '../../shared/services/util.service';
import { Api2Service } from 'src/app/shared/services/api2.service';
import { Subscription, Observable } from 'rxjs';
import { NewsService } from 'src/app/shared/services/news.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {

  dia: any = [];
  semana: any = [];
  geral: any = [];
  eletro: any = [];
  campanha: any = [];
  show: boolean = true;
  ofertas: Observable<any>;
  imagem: Observable<any>;
  subscriptionFakes: Subscription;

  constructor(private v2: Api2Service, private news: NewsService) {}

  ngOnInit(): void {
    this.imagem = this.news.bannerCarousel();
    this.subscriptionFakes = this.v2.getFakes().subscribe(res => {
      const offer = res.ofertas;
      this.dia = offer[0];
      this.semana = offer[1];
      this.geral = offer[2];
      this.eletro = offer[3];
      this.campanha = offer[4];
    });
    this.show = true;
  }

  ngOnDestroy(): void {
    if(this.subscriptionFakes) { this.subscriptionFakes.unsubscribe(); }
  }
}
