import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LojasComponent } from './lojas.component';
import { DetailComponent } from './detail/detail.component';
import { FormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgxPaginationModule } from 'ngx-pagination';
import { Routes, RouterModule } from '@angular/router';
import { HelperModule } from 'src/app/shared/helper/helper.module';

const app: Routes = [
  { path: '', component: LojasComponent },
  { path: ':slug', component: DetailComponent }
];


@NgModule({
  declarations: [LojasComponent, DetailComponent],
  imports: [
    FormsModule,
    CommonModule,
    HelperModule,
    MDBBootstrapModule,
    NgxPaginationModule,
    RouterModule.forChild(app)
  ]
})
export class LojasModule { }
