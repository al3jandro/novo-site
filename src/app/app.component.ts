import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { Api2Service } from './shared/services/api2.service';
import { UtilService } from './shared/services/util.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  title = 'reload';
  items: Observable<any>;
  init: boolean = false;
  noscroll: boolean = false;

  constructor(
    router: Router,
    util: UtilService,
    gtmService: GoogleTagManagerService,
    private v2: Api2Service
  ) {
    if (!util.StorageParse('Loja')) {
      this.init = true;
    }
    router.events.forEach((item) => {
      if (item instanceof NavigationEnd) {
        const gtmTag = { event: 'page', pageName: item.url };
        gtmService.pushTag(gtmTag);
      }
    });
  }

  ngOnInit(): void {
    this.items = this.v2.Fakes();
  }

  onActivate(event) {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) { window.scrollTo(0, pos - 20); }
      else { window.clearInterval(scrollToTop); }
    }, 16);
  }

  target(e: any) {
    this.noscroll = e;
  }
}
