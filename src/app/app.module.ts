
import { NgModule, Injectable } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgtUniversalModule } from '@ng-toolkit/universal';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { APP_ROUTE } from './app.routes';
import { EXTERNAL_ROUTES } from './ext.routes';
import { HeaderModule } from './shared/components/header/header.module';
import { FooterModule } from './shared/components/footer/footer.module';
import { TopModule } from './shared/components/top/top.module';
import { MenuModule } from './shared/components/menu/menu.module';
import { HelperModule } from 'src/app/shared/helper/helper.module';
import { MobileModule } from './shared/components/mobile/mobile.module';
import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';

registerLocaleData(localePt);

declare var Hammer: any;

@Injectable()
export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any> {
    pan: { direction: Hammer.DIRECTION_All },
    swipe: { direction: Hammer.DIRECTION_VERTICAL },
  } as any;
  buildHammer(element: HTMLElement) {
    const mc = new Hammer(element, {
      touchAction: 'auto',
      inputClass: Hammer.SUPPORT_POINTER_EVENTS
        ? Hammer.PointerEventInput
        : Hammer.TouchInput,
      recognizers: [[Hammer.Swipe, { direction: Hammer.DIRECTION_HORIZONTAL }]],
    });
    return mc;
  }
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    APP_ROUTE,
    EXTERNAL_ROUTES,
    HttpClientModule,
    NgtUniversalModule,
    TopModule,
    HeaderModule,
    MobileModule,
    MenuModule,
    FooterModule,
    HelperModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    BrowserModule.withServerTransition({ appId: 'condor-web' }),
    ServiceWorkerModule.register('ngsw-worker.js',
      { enabled: environment.production, registrationStrategy: 'registerImmediately' })
  ],
  providers: [
    { provide: HAMMER_GESTURE_CONFIG, useClass: MyHammerConfig },
    { provide: 'googleTagManagerId', useValue: 'GTM-T7FLP2C' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
