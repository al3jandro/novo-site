import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { tap, take, retry, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Ofertas } from './interfaces/ofertas';

const url = environment.v1.url;
const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private http: HttpClient) { }

  add(table: string, data: any): Observable<any[]> {
    return this.http.post<any[]>(`${url}/${table}`, data, { headers });
  }

  private Query<T>(query: string) {
    return this.http.get<T>(url + query);
  }

  /** Menus */
  getMenuOfertas(tipo: string): Observable<any[]> {
    return this.Query<any[]>(`/Produtos/${tipo}`);
  }

  getCollection(collection: string): Observable<any[]> {
    return this.Query<any[]>(collection).pipe(retry(3), map((res) => res));
  }

  /** Ofertas */
  // Ofertas Loja Departamento
  OfertasLojaDepartamento(loja: number, departamento: number, limit: number = 100): Observable<Ofertas[]> {
    return this.Query<Ofertas[]>(
      `/Ofertas/LojaProdutosDepartamento?loja=${loja}&departamento=${departamento}&limit=${limit}`).pipe(retry(3), map((res) => res));
  }
  // Ofertas Loja Slug
  OfertasLojaSlug(loja: number, slug: string, limit: number = 100): Observable<Ofertas[]> {
    return this.Query<Ofertas[]>(`/Ofertas/LojaProdutosSlug?loja=${loja}&slug=${slug}&limit=${limit}`).pipe(retry(3), map((res) => res));
  }
  // Ofertas Loja Campanha
  OfertasLojaCampanha(loja: number, campanha: number, limit: number = 100): Observable<Ofertas[]> {
    return this.Query<Ofertas[]>(`/Ofertas/LojaProdutosCampanha?loja=${loja}&campanha=${campanha}&limit=${limit}`).pipe(retry(3), map((res) => res));
  }

  // Ofertas Loja Departamento
  OfertasLojaDepartamentoSetor(loja: number, departamento: number, setor: number, limit: number = 100): Observable<Ofertas[]> {
    const query = `/Ofertas/LojaProdutosDepartamentoSetor?loja=${loja}&departamento=${departamento}&setor=${setor}`;
    return this.Query<Ofertas[]>(`${query}&limit${limit}`).pipe(retry(3), map((res) => res));
  }

  // Search
  Search(search: string): Observable<any[]> {
    const query1 = `filter[where][or][0][dsc_produto][like]=%25${search}%25`;
    const query2 = `filter[where][or][1][dsc_descricao][like]=%25${search}%25`;
    const query = `/Produtos?filter[include]=ofertas&${query1}&${query2}`;
    return this.Query<any[]>(query).pipe(retry(3), map((res) => res));
  }

  // Produto
  ProdutoLoja(loja: number, host: number) {
    return this.Query<any[]>(`/Ofertas/LojaProduto?loja=${loja}&host=${host}`).pipe(retry(3), map((res) => res[0]));
  }
}
