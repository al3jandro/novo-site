import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Meta, Title } from '@angular/platform-browser';

declare global {
  interface Window { dataLayer: any[]; }
}
@Injectable({providedIn: 'root' })

export class SeoService {
  constructor(
    private meta: Meta,
    private title: Title,
    @Inject(DOCUMENT) private doc,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.meta.addTags([
      { name: 'og:type', content: 'website' },
      { name: 'twitter:card', content: 'summary_large_image' }
    ]);
  }

  setTitle(titulo: string) {
    return this.title.setTitle(titulo);
  }

  addTagName(content: string) {
    return this.meta.addTags([
      { name: 'title', content },
      { name: 'og:title', content },
      { name: 'twitter:title', content }
    ]);
  }


  updateTagName(content: string) {
    this.meta.updateTag({ name: 'title', content: content} );
    this.meta.updateTag({ name: 'og:title', content: content} );
    this.meta.updateTag({ name: 'twitter:title', content: content} );
  }

  addTagDescription(content: string) {
    return this.meta.addTags([
      { name: 'description', content },
      { name: 'og:description', content },
      { name: 'twitter:description', content }
    ]);
  }

  updateTagDescription(content: string) {
    this.meta.updateTag({ name: 'description', content: content} );
    this.meta.updateTag({ name: 'og:description', content: content} );
    this.meta.updateTag({ name: 'twitter:description', content: content} );
  }


  addCanonical() {
    if (isPlatformBrowser(this.platformId)) {
      const link: HTMLLinkElement = this.doc.createElement('link');
      link.setAttribute('rel', 'canonical');
      this.doc.head.appendChild(link);
      link.setAttribute('href', this.doc.URL);
    }
  }


  addTagImage(content: string) {
    return this.meta.addTags([
      { name: 'og:image', content },
      { name: 'twitter:image', content }
    ]);
  }

  updateTagImage(content: string) {
    this.meta.updateTag({ name: 'og:image', content: content} );
    this.meta.updateTag({ name: 'twitter:image', content: content} );
  }

  addGeolocation(cidade: string, latLng: string) {
    let uf: any;
    if (cidade === 'Curitiba') {
      uf = 'BR-PR';
    } else {
      uf = 'BR-SC';
    }
    return this.meta.addTags([
      { name: 'geo.region', content: uf },
      { name: 'geo.placename', content: cidade },
      { name: 'geo.position', content: latLng },
      { name: 'ICBM', content: latLng }
    ]);
  }

  dataLayerPost(pageType?: string, contentCategory?: string, pageTitle?: string) {
    if (isPlatformBrowser(this.platformId)) {
      return window.dataLayer.push({
        event: 'page',
        pageType,
        contentCategory,
        pageTitle
      });
    } else {
      return true;
    }
  }


  dataLayerBlog(arr: any) {

    if (isPlatformBrowser(this.platformId)) {
      return window.dataLayer.push(arr);
    } else {
      return true;
    }
  }

  dataLayerProduct(arr: any) {
    if (isPlatformBrowser(this.platformId)) {
      return window.dataLayer.push(arr);
    } else {
      return true;
    }
  }

  dataLayerTracking(arr: any) {
    if (isPlatformBrowser(this.platformId)) {
      return window.dataLayer.push(arr);
    } else {
      return true;
    }
  }
}
