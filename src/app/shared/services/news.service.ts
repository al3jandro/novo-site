import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Imagens, Page, Loja, Post, Tabloide } from './interfaces/news';
import { Observable, forkJoin } from 'rxjs';
import { tap, map, retry } from 'rxjs/operators';
import { Banners } from './interfaces/reactive';

const url: string = environment.news.url;
const headers = new HttpHeaders({Authorization: `${ environment.news.key }`});
const urlBlog: string = environment.blog.url;
const headersBlog = new HttpHeaders({Authorization: `${ environment.blog.key }`});

@Injectable({
  providedIn: 'root'
})

export class NewsService {

  constructor(private http: HttpClient) { }

  getImage(id: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${url}/media/${id}`, { headers })
      .pipe(tap(data => data));
  }

  /** Module Page */
  Page(): Observable<Page[]> {
    return this.http.get<Page[]>(`${url}/pages`)
    .pipe(tap(data => data));
  }

  LastNews(limit: number): Observable<Post[]> {
    return this.http.get<Post[]>(`${url}/posts`).pipe(retry(3), map(res => res), map((data) => data['slice'](0,limit)));
  }

  PageCollection(collection: string): Observable<Page> {
    return this.http.get<Page>(`${url}/pages${collection}`).pipe(retry(3), map(res => res));
  }

  PageSlug(slug: string): Observable<Page> {
    return this.http.get<Page>(`${url}/pages`, { params: { slug: `${slug}` }})
    .pipe(
      retry(3),
      map((res) => res[0])
      );
  }

  PageId(id: number): Observable<Page[]> {
    const sql = `${url}/pages`;
    return this.http.get<Page[]>(`${url}/pages/${id}`)
    .pipe(tap(data => data));
  }

  /** Module Post */
  Posts(p: number, page: number): Observable<any> {
    return this.http.get<any>(`${url}/posts?page=${p}&per_page=${page}`, {observe: 'response'})
    .pipe(tap(data => data));
    }

  PostSlug(slug: string): Observable<Post[]> {
    return this.http.get<Post[]>(`${url}/posts`, {
      params: { slug: `${slug}` }
    }).pipe(tap(data => data));
  }

  PostId(id: number): Observable<Post[]> {
    return this.http.get<Post[]>(`${url}/posts/${id}`)
    .pipe(tap(data => data));
  }

  PostSearch(search: string): Observable<Post[]> {
    return this.http.get<Post[]>(`${url}/posts`, { params: { search: `${search}`}, headers
    }).pipe(tap(data => data));
  }

  /** Module Imagens */
  ImagensPosition(position: string): Observable<Imagens[]> {
    return this.http.get<Imagens[]>(`${url}/imagens`, {
      params: { position }, headers
    }).pipe(
      retry(3),
      map(res => res.filter(row => row.status === 'publish' || row.position == position))
    );
  }

  Imagens(perPage: number = 100): Observable<Imagens[]> {
    const sql = `${url}/imagens`;
    return this.http.get<Imagens[]>(`${url}/imagens`, {
      params: { per_page: `${perPage}` }
    }).pipe(tap(data => data));
  }

  bannerCarousel(): Observable<Banners> {
    return this.http.get<Banners>(`${url}/imagens?per_page=100`).pipe(
      map(res => {
        return {
          topLeft: res['filter'](row => row.position == 'top-left'),
          topRight: res['filter'](row => row.position == 'top-right'),
          middleLeft: res['filter'](row => row.position == 'middle-left'),
          middleCenter: res['filter'](row => row.position == 'middle-center'),
          middleRight: res['filter'](row => row.position == 'middle-right')
        }
      })
    );
  }

  /** Module Campanha */
  Campanha(slug: string): Observable<any[]> {
    return this.http.get<any[]>(`${url}/posto`, {params: { slug: `${slug}` }})
    .pipe(map(data => data[0]));
  }

  /** Module Loja */
  Lojas(limit: number): Observable<Loja[]> {
    return this.http
      .get<Loja[]>(`${url}/loja`, { params: { per_page: `${limit}` }, headers })
      .pipe(tap(data => data));
  }

  LojaSlug(slug: string): Observable<Loja[]> {
    return this.http
      .get<Loja[]>(`${url}/loja`, { params: { slug: `${slug}` }, headers })
      .pipe(tap(data => data));
  }

  LojaId(id: number): Observable<Loja[]> {
    return this.http
      .get<Loja[]>(`${url}/loja/${id}`)
      .pipe(tap(data => data));
  }

  LojaCodigo(id: any): Observable<Loja[]> {
    return this.http
      .get<Loja[]>(`${url}/loja`, {
        params: { meta_key: 'cod_loja', meta_value: `${id}` }, headers
      }).pipe(tap(data => data));
  }

  lojasXregion() {
    return forkJoin(
      {
        region: this.http.get<any[]>(`${url}/region`, { params: { per_page: `100`}, headers }).pipe(map(data => data)),
        lojas: this.http.get<Loja[]>(`${url}/loja`, { params: { per_page: `100`}, headers }).pipe(map(data => data))
      }
    );
    // return this.http.get<any[]>(`${url}/region`, { params: { per_page: `100` }, headers }).pipe(retry(3), map(data => data));
  }

  LojaRegion() {
    return this.http.get<any[]>(`${url}/region`, { params: { per_page: `100` }, headers }).pipe(retry(3), map(data => data));
  }

  LojaPorRegion(id: number): Observable<Loja[]> {
    return this.http
      .get<Loja[]>(`${url}/loja`, {
        params: { region: `${id}` }, headers
      }).pipe(tap(data => data));
  }

  /** Tabloide */
  Tabloides(limit: number): Observable<Tabloide[]> {
    return this.http
      .get<Tabloide[]>(`${url}/tabloide`, { params: { per_page: `${limit}` }
      }).pipe(tap(data => data));
  }

  TabloideSlug(slug: string): Observable<Tabloide[]> {
    return this.http
      .get<Tabloide[]>(`${url}/tabloide`, { params: { slug: `${slug}` }, headers })
      .pipe(tap(data => data));
  }

  TabloideId(id: number): Observable<Tabloide[]> {
    return this.http
      .get<Tabloide[]>(`${url}/tabloide/${id}`)
      .pipe(tap(data => data));
  }

  /** Post Blog */
  Blog(p: number, page: number): Observable<any> {
    return this.http
      .get<any>(`${urlBlog}/posts?page=${p}&per_page=${page}`, {observe: 'response'}).pipe(map(data => data));
  }

  BlogId(id: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${ urlBlog }/posts/${id}`, {headers: headersBlog})
      .pipe(tap(data => data));
  }

  BlogSlug(slug: string): Observable<any[]> {
    return this.http
      .get<any[]>(`${ urlBlog }/posts`,
      { params: { slug: `${slug}` }, headers: headersBlog})
      .pipe(tap(data => data));
  }

  BlogImage(id: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${ urlBlog }/media/${id}`, {headers: headersBlog})
      .pipe(tap(data => data));
  }

  getBlogCollection(collection: string) {
    return this.http.get<any[]>(`${ urlBlog }/${collection}`, {observe: 'response'}).pipe(map(data => data));
  }
}
