export interface Banners {
  topLeft?: Imagens[];
  topRight?: Imagens[];
  middleLeft?: Imagens[];
  middleCenter?: Imagens[];
  middleRight?: Imagens[];
}


export interface Imagens {
  id: number;
  date?: string;
  slug?: string;
  status?: string;
  link?: string;
  title?: string;
  featured_media?: number;
  url?: string;
  position?: string;
  thumbnail?: string;
  medium?: string;
  full?: string;
}
