import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { MenuDepartamento } from './interfaces/menu';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

const url = environment.v1.url;
const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});

@Injectable({
  providedIn: 'root'
})

export class Api2Service {

  private fake$: BehaviorSubject<any> = new BehaviorSubject(null);
  private ofertas$: BehaviorSubject<any> = new BehaviorSubject(null);
  private menu$: BehaviorSubject<MenuDepartamento> = new BehaviorSubject(null);

  constructor(private http: HttpClient) { }

  private Query<T>(query: string) {
    return this.http.get<T>(url + query);
  }

  getFakes(): Observable<any> { return this.fake$.asObservable(); }
  setFakes(items: any) { this.fake$.next(items); }

  getOfertas(): Observable<any> { return this.ofertas$.asObservable(); }
  setOfertas(items: any) { this.ofertas$.next(items); }

  setMenu(items: any) { this.fake$.next(items); }
  getMenu(): Observable<any> { return this.ofertas$.asObservable(); }

  Fakes() {
    return this.http.get(`./assets/data/database.json`)
    .pipe(
      map(res => {
        this.setFakes(res[0]);
        return {
          headers: res[0].headers,
          top: res[0].top,
          menu: res[0].menu,
          ofertas: res[0].ofertas,
          footers: res[0].footers
        }
      })
    );
  }

  getCollection(collection: string): Observable<any[]> {
    return this.Query<any[]>(collection).pipe(map((res) => res));
  }
}
