import { Component, OnInit, Output, Input, ViewChild, OnDestroy } from '@angular/core';
import { NewsService } from '../../../shared/services/news.service';
import { Subscription, Observable, timer } from 'rxjs';
import { SeoService } from '../../../shared/services/seo.service';
import { UtilService } from '../../../shared/services/util.service';
import { Loja } from 'src/app/shared/services/interfaces/news';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api.service';

const objeto = { loja: 21, slug: 'hiper-condor-nilo-pecanha', nome: 'Hiper Condor Nilo Peçanha' };

@Component({
  selector: 'helper-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent implements OnInit, OnDestroy {

  @Output() loja: any;
  @Input() new: any;
  @Input() type?: string = 'simple';
  @Input() load?: boolean = false;
  @ViewChild('frame', { static: true })
  public frame: any;
  region: any = [];
  data: any = [];
  lojas: any = [];
  miObjeto: any = [];
  condor: number;

  private lojaSubscription: Subscription;

  constructor(
    private news: NewsService,
    private router: Router,
    private seo: SeoService,
    private api: ApiService,
    private util: UtilService
  ) { }

  ngOnInit() {

    timer(800).subscribe(() => {
      this.active();
      if (this.load === true) {
        this.frame.show();
        this.getData();
       }
    })
  }

  getData() {
    this.news.lojasXregion().subscribe(res => {
      this.region = res.region;
      this.data = res.lojas;
    });
  }

  active() {
    if (this.util.StorageParse('Loja')) {
      this.miObjeto = this.getLocalStorageLoja();
    }
  }

  ngOnDestroy(): void {
    if (this.lojaSubscription) {this.lojaSubscription.unsubscribe(); }
  }

  open() {
    this.frame.show();
    this.getData();
    this.seo.dataLayerTracking({
      event: 'findStore',
      storeAction: 'Clique Header',
      storeName: 'Nome da Loja'
    });
  }

  close() {
    this.frame.hide();
  }

  setLocalStorageLoja() {
    this.util.StorageAddKey('Loja', objeto);
  }

  getLocalStorageLoja() {
    return this.util.StorageParse('Loja');
  }

  getLojas(e: any) {
    const filtro = this.data.filter((row: any) => this.util.toSlug(row.cidade) === e.target.value)
    this.lojas = filtro;
  }

  selectCondor() {
    this.lojaSubscription = this.news.LojaId(this.condor).subscribe(data => {
      const cod = data['condor']['cod_loja'][0];
      this.miObjeto = {
        loja: cod,
        slug: data['slug'],
        nome: data['title'].rendered
      };
      this.seo.dataLayerTracking({
        event: 'findStore',
        storeAction: 'Selecionar Cidade',
        storeCity: data['cidade'],
        storeName: data['title'].rendered
      });
      this.util.StorageRemoveKey('Loja');
      this.util.StorageAddKey('Loja', this.miObjeto);
    });
    this.frame.hide();
    setTimeout(() => {
      this.init();
    }, 300);
  }

  init() {
    const www = 'https://www.condor.com.br';
    const url = this.router.url.split('/');
    const value = url[url.length - 1 ];
    if (value.split('?').shift() === 'init') {
      if (url[url.length - 3 ] === 'departamento') {
        return window.location.href = `${www}/${url[1]}/${url[2]}`;
      } else if (url[url.length - 3 ] === 'setor') {
        return window.location.href = `${www}/${url[1]}/${url[2]}/${url[3]}/${url[4]}`;
      } else {
        return window.location.href = `${www}/produto/${url[2]}/${url[3]}/${url[4]}`;
      }
    } else {
      return window.location.reload();
    }
  }
}
