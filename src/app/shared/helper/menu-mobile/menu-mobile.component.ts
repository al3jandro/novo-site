import { timer } from 'rxjs';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'helper-menu-mobile',
  templateUrl: './menu-mobile.component.html',
  styleUrls: ['./menu-mobile.component.scss']
})
export class MenuMobileComponent implements OnInit {

  @Input() menu: any = [];
  @Input() titulo?: string = '';
  @Input() active?: boolean = true;
  @Output() fechar = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit(): void {
    // timer(300).subscribe(() => console.log(this.menu));
  }
  submenu() {
    this.active = !this.active;
  }

  toogle() {
    this.fechar.emit(false);
  }

}
