import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageDirective } from './image.directive';
import { ProductDirective } from './product.directive';


@NgModule({
  declarations: [ProductDirective, ImageDirective],
  exports: [ProductDirective, ImageDirective],
  imports: [
    CommonModule
  ]
})
export class DirectiveModule { }
