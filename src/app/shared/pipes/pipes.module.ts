import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltroPipe } from './filtro.pipe';
import { TruncatePipe } from './truncate.pipe';
import { SafePipe } from './safe.pipe';
import { SortPipe } from './sort.pipe';
import { LojafilterPipe } from './lojafilter.pipe';


@NgModule({
  declarations: [FiltroPipe, TruncatePipe, SafePipe, SortPipe, LojafilterPipe],
  exports: [FiltroPipe, TruncatePipe, SafePipe, SortPipe, LojafilterPipe],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
