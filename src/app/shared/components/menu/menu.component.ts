import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MenuDepartamento } from '../../services/interfaces/menu';
import { ApiService } from '../../services/api.service';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

export class MenuComponent implements OnInit, OnDestroy {

  @Input() items: any = [];
  menu: MenuDepartamento[] = [];
  subscription: Subscription;

  constructor( private api: ApiService ) { }

  ngOnInit(): void {
    this.subscription = this.api.getMenuOfertas('menuDepartamento').subscribe(
      res => {
        for (const key in this.items.campanha) {
          res.push({
            codigo: this.items.campanha[key].codigo,
            nome: this.items.campanha[key].nome,
            slug: this.items.campanha[key].slug,
            campanha: this.items.campanha[key].campanha,
            external: this.items.campanha[key].external
          });
        }
        this.menu = res;
      }
    );
  }

  ngOnDestroy(): void {
    if (this.subscription) { this.subscription.unsubscribe();}
  }
}
