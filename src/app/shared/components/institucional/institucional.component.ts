import { NewsService } from 'src/app/shared/services/news.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-institucional',
  templateUrl: './institucional.component.html',
  styleUrls: ['./institucional.component.scss']
})
export class InstitucionalComponent implements OnInit {

  items: any = [];

  constructor( private news: NewsService ) { }

  ngOnInit(): void {
    this.Pages();
  }

  Pages() {
    const query = `?include[]=5&include[]=10&include[]=12&include[]=14`;
    this.items = this.news.PageCollection(query);
  }

}
