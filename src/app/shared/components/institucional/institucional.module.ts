import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstitucionalComponent } from './institucional.component';
import { RouterModule } from '@angular/router';
import { HelperModule } from './../../helper/helper.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { DirectiveModule } from '../../directive/directive.module';
import { PipesModule } from '../../pipes/pipes.module';



@NgModule({
  declarations: [InstitucionalComponent],
  bootstrap: [InstitucionalComponent],
  exports: [InstitucionalComponent],
  imports: [
    DirectiveModule,
    PipesModule,
    CommonModule,
    RouterModule,
    HelperModule,
    MDBBootstrapModule
  ]
})
export class InstitucionalModule { }
