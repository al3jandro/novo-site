import { Component, OnInit, Input } from '@angular/core';
import { Api2Service } from 'src/app/shared/services/api2.service';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @Input() items: any = [];

  constructor(private v2: Api2Service) {}

  ngOnInit() { }

}
