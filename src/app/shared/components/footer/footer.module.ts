import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RouterModule } from '@angular/router';
import { HelperModule } from './../../helper/helper.module';

@NgModule({
  declarations: [FooterComponent],
  bootstrap: [FooterComponent],
  exports: [FooterComponent],
  imports: [
    CommonModule,
    RouterModule,
    HelperModule,
    MDBBootstrapModule
  ]
})
export class FooterModule { }
