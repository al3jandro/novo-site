import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { trigger, transition, animate, style } from '@angular/animations';
import { Router, NavigationEnd } from '@angular/router';
import {filter, map, tap} from 'rxjs/operators';
import { Observable, timer } from 'rxjs';
import { NewsService } from '../../services/news.service';
import { SeoService } from '../../services/seo.service';
import { ApiService } from '../../services/api.service';
import { UtilService } from '../../services/util.service';


@Component({
  selector: 'app-mobile',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.scss'],
  animations: [
    trigger('slideInRight', [
      transition(':enter', [
        style({transform: 'translateX(-100%)'}),
        animate('200ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({transform: 'translateX(-100%)'}))
      ])
    ])
  ]
})
export class MobileComponent implements OnInit {

  @Input() items: any = [];
  @Input() entrada: boolean = false;
  @Output() salida = new EventEmitter<boolean>();

  active: boolean = true;
  active1: boolean = true;
  especiais: boolean = true;
  activeBlog: boolean = false;
  top: any = [];
  ofertas: any = [];
  category: any = [];
  departamento: Observable<any[]>;
  @ViewChild('search') search: ElementRef;


  constructor(
    private news: NewsService,
    private seo: SeoService,
    private api: ApiService,
    private util: UtilService,
    private router: Router
  ) {
    router.events.pipe(filter(event => event instanceof NavigationEnd))
    .subscribe(event => {
      const url = event['url'].split('/').find((element: any) => element === 'blog');
      if (url) {
        this.activeBlog = true;
      } else {
        this.activeBlog = false;
      }
    });
  }

  ngOnInit(): void {
    timer(100).subscribe(() => this.top = this.items.top);
    this.ofertas = this.api.getMenuOfertas('menuDepartamento');
    this.Category();
  }


  test(e) { this.entrada = e; }

  toogle() {
    this.entrada = !this.entrada;
    this.salida.emit(this.entrada);
  }

  submenu() { this.active = !this.active; }
  submenu1() { this.active1 = !this.active1; }
  toogleEspeciais() { this.especiais = !this.especiais; }

  getSearch(event: any) {
    if (event.keyCode === 13) {
      if (! event.target.value) { return; }
      this.router.navigate(['pesquisa-usuario', this.util.toSlug(event.target.value)]);
    }
  }

  getSearchClick() {
    if (! this.search.nativeElement.value) { return; }
    this.router.navigate(['pesquisa-usuario', this.util.toSlug(this.search.nativeElement.value)]);
  }

  Category() {
    const query = 'categories?orderby=count&order=desc&per_page=10';
    this.category = this.news.getBlogCollection(query).pipe(map(res => res['body']));
  }

  goToCategory(e: any) {
    this.router.navigate(['/blog', 'categoria', e.target.value]);
  }

  goToDepartamento(e: any) {
    this.router.navigate(['/departamento', e.target.value]);
  }


  Click(event) {
    if (event.keyCode === 13) {
      if (! event.target.value) { return; }
      this.seo.dataLayerTracking({ event: 'blogsearch', searchTerm: event.target.value });
      return this.router.navigate(['blog', 'search', this.util.toSlug(event.target.value)]);
    }
  }
}
