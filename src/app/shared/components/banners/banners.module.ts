import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannersComponent } from './banners.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RouterModule } from '@angular/router';
import { HelperModule } from './../../helper/helper.module';
import { DirectiveModule } from '../../directive/directive.module';



@NgModule({
  declarations: [BannersComponent],
  bootstrap: [BannersComponent],
  exports: [BannersComponent],
  imports: [
    DirectiveModule,
    HelperModule,
    CommonModule,
    RouterModule,
    MDBBootstrapModule
  ]
})
export class BannersModule { }
