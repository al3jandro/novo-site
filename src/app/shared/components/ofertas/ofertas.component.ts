import { Component, OnInit, Input } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Subscription, Observable, timer } from 'rxjs';
import { Ofertas } from 'src/app/shared/services/interfaces/ofertas';
import { map } from 'rxjs/operators';
import { Api2Service } from 'src/app/shared/services/api2.service';
import { LojaStorage, UtilService } from '../../services/util.service';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.component.html',
  styleUrls: ['./ofertas.component.scss'],
})
export class OfertasComponent implements OnInit {
  @Input() code: any;
  @Input() sector?: number;
  @Input() type?: any;
  @Input() title: string;
  @Input() icon: string;
  @Input() link: string;
  @Input() data: any = [];


  items: Observable<any[]>;
  test: Observable<Ofertas>;
  loja: LojaStorage;
  subscription: Subscription;
  timerSubs: Subscription;

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    dots: false,
    navSpeed: 500,
    autoWidth:false,
    margin: 10,
    autoplay: true,
    nav: true,
    navText: [
      '<i class="fas fa-chevron-left"></i>',
      '<i class=" fas fa-chevron-right"></i>',
    ],
    responsive: {
      0: { items: 2, nav: true },
      480: { items: 3, nav: true },
      768: { items: 3, nav: true },
      1024: { items: 4, nav: true, mergeFit:false },
    },
  };

  constructor(private api: ApiService, private util: UtilService, private v2: Api2Service) {}

  ngOnInit(): void {
    this.loja = this.util.StorageParse('Loja');
    this.timerSubs = timer(300).subscribe(() => {
      if (!this.loja) { return; }
      switch (this.type) {
        case 'slug': {
          this.Slug(this.code, this.loja.loja);
          break;
        }
        case 'campanha': {
          this.Campanha(this.code, this.loja.loja);
          break;
        }
        case 'sector': {
          this.Sector(this.code, this.sector, this.loja.loja);
          break;
        }
        default: {
          this.Departamento(this.code, this.loja.loja);
          break;
        }
      }
    })
  }

  ngOnDestroy(): void {
    if(this.timerSubs) { this.timerSubs.unsubscribe();}
  }

  Slug(code: any, loja: number = 6, limit: any = 15) {
    this.items = this.v2.getCollection(`/Ofertas/LojaProdutosSlug?loja=${loja}&slug=${code}&limit=${limit}`)
  }

  Campanha(code: any, loja: number = 6, limit: any = 15) {
    this.items = this.v2.getCollection(`/Ofertas/LojaProdutosCampanha?loja=${loja}&campanha=${code}&limit=${limit}`)
  }

  Departamento(code: any, loja: number = 6, limit: any = 15) {
    this.items = this.v2.getCollection(`/Ofertas/LojaProdutosDepartamento?loja=${loja}&departamento=${code}&limit=${limit}`)
  }

  Sector(departamento: number, setor: number, loja: number = 6, limit: any = 15) {
    this.items = this.v2.getCollection(`/Ofertas/LojaProdutosDepartamentoSetor?loja=${loja}&departamento=${departamento}&setor=${setor}&limit=${limit}`)
    .pipe(map((res) => {
      const arr = [];
      res.forEach(el => {
        if(el.produtos){ arr.push(el); }
      })
      return arr;
    }));
  }
}
