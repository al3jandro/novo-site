import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { NewsService } from '../../services/news.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})

export class PostComponent implements OnInit {

  items: Observable<any[]>;

  constructor(private news: NewsService) { }

  ngOnInit(): void {
    this.items = this.news.Blog(1, 3).pipe(map(res => res.body));
  }
}
