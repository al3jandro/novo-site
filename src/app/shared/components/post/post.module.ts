import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostComponent } from './post.component';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { RouterModule } from '@angular/router';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HelperModule } from '../../helper/helper.module';



@NgModule({
  declarations: [PostComponent],
  bootstrap: [PostComponent],
  exports: [PostComponent],
  imports: [
    PipesModule,
    CommonModule,
    RouterModule,
    HelperModule,
    MDBBootstrapModule
  ]
})
export class PostModule { }
