import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from '../../services/util.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  keyword = 'loja';
  @Input() items: any = [];

  constructor(
    private util: UtilService,
    private router: Router,
  ) { }


  ngOnInit() { }

  getSearch(event: any) {

    if (event.keyCode === 13) {
      return this.router.navigate(['pesquisa-usuario', this.util.toSlug(event.target.value)]);
    }
  }
}
