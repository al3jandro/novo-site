
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { timer } from 'rxjs';
import { ApiService } from '../../services/api.service';
import { UtilService } from '../../services/util.service';


@Component({
    selector: 'app-top',
    templateUrl: './top.component.html',
    styleUrls: ['./top.component.scss']
})
export class TopComponent implements OnInit {

  @Input() items: any = [];
    @Input() entrada: boolean = false;
    @Output() salida = new EventEmitter<boolean>();

    active1: boolean = true;
    active2: boolean = true;
    toogle1: boolean = false;

    json: any = [];
    loja: any = [];
    ofertas: any = [];

    constructor(
        private api: ApiService,
        private util: UtilService) { }

    ngOnInit(): void {
      timer(300).subscribe(() => {
        this.json = this.items.top;
        const menu = this.items.menu.campanha;
        this.api.getMenuOfertas('menuDepartamento').subscribe(offer => {
          for (const key in menu) {
            offer.push({
              codigo: menu[key].codigo,
              nome: menu[key].nome,
              slug: menu[key].slug,
              campanha: menu[key].campanha
            });
          }
          this.ofertas = offer;
        });
      });
        this.loja = this.util.StorageParse('Loja');
    }

    active() { this.toogle1 = !this.toogle1; }
    submenu1() { this.active1 = !this.active1; }
    submenu2() { this.active2 = !this.active2; }

    toogle() {
        this.entrada = !this.entrada;
        this.salida.emit(this.entrada);
    }
}
