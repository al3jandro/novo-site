export const environment = {
  production: true,
  url: 'https://marketing.condor.com.br',
  v1: {
    url: 'https://marketing.condor.com.br/api',
    key: ''
  },
  news: {
    url: 'https://teste.condor.com.br/news/wp-json/wp/v2',
    key: 'Basic Y29uZG9yOmFkbWluMTIzIUAj'
  },
  blog: {
    url: 'https://teste.condor.com.br/blog/wp-json/wp/v2',
    key: 'Basic Y29uZG9yOmFkbWluMTIz',
  }
};
